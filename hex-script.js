const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');

const xDist = 2;
const yDist = 2;
const shape = [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 0 }, { x: 1, y: 1 }];

const colours = ["#FFF", "#000"];

const width = Math.floor(canvas.width / xDist) - 2;
const height = Math.floor(canvas.height / yDist) - 2;

const cx = Math.floor(width / 2);
const cy = Math.floor(height / 2);

const tickMult = 10;

var x;
var y;
var dir;
var grid;
var running;

function setup() {
	grid = new Array(width).fill(0).map(i => new Array(height).fill(0));
	x = cx;
	y = cy;
	dir = 0;
	running = true;

	window.requestAnimationFrame(tick);
	//canvas.onclick = tick;
}

function tick() {
	for (i = 0; i < tickMult; i++) {
		step();
		move();
		turn();
	}
	
	if (running) window.requestAnimationFrame(tick);
}

function step() {
	grid[x][y] = (grid[x][y] + 1) % 2;
	ctx.fillStyle = colours[grid[x][y]];
	var xPos = (x + 1) * xDist;
	var yPos = (y + (x % 2 == 0 ? 1 : 1.5)) * yDist;
	for (var p of shape) ctx.fillRect(xPos + p.x, yPos + p.y, 1, 1);
}

function move() {
	switch (dir) {
		case 0: y -= 1; break;
		case 1: if (x % 2 == 0) y -= 1; x += 1; break;
		case 2: if (x % 2 == 1) y += 1; x += 1; break;
		case 3: y += 1; break;
		case 4: if (x % 2 == 1) y += 1; x -= 1; break;
		case 5: if (x % 2 == 0) y -= 1; x -= 1; break;
	}
	running = x >= 0 && x < width && y >= 0 && y < height;
}

function turn() {
	var rot;
	switch (grid[x][y]) {
		case 0: dir += 1; break;
		case 1: dir -= 1; break;
	}
	dir = ((dir % 6) + 6) % 6;
}

setup();
